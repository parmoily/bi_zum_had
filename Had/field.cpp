#include "field.h"

Field::Field(QObject *parent) :
    QObject(parent)
{
}

int Field::getWidth() const
{
    return width;
}

void Field::setWidth(int value)
{
    width = value;
}

int Field::getHeight() const
{
    return height;
}

void Field::setHeight(int value)
{
    height = value;
}

void Field::removeFood(QPair<int, int> pos)
{
    foodPos.removeOne(pos);
}

void Field::addFood(QPair<int, int> pos)
{
    foodPos.append(pos);
    emit foodCreated(pos.first, pos.second);
}

QList<QPair<int, int>> Field::getFoodPos() const
{
    return foodPos;
}

