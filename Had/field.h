#ifndef FIELD_H
#define FIELD_H

#include <QObject>

class Field : public QObject
{
    Q_OBJECT
signals:
    void foodCreated(int h, int w);
public:
    explicit Field(QObject *parent = nullptr);
    int getWidth() const;
    void setWidth(int value);

    int getHeight() const;
    void setHeight(int value);

    void addFood(QPair<int, int> pos);
    QList<QPair<int, int>> getFoodPos() const;
    void removeFood(QPair<int, int> pos);

private:

    int width = 0;
    int height = 0;
    QList<QPair<int, int>> foodPos;
signals:

};

#endif // FIELD_H
