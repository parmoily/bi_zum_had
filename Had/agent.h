#ifndef AGENT_H
#define AGENT_H

class Agent
{
public:
    explicit Agent();
    virtual void makeDecision() = 0;
    virtual void start() = 0;
    virtual void stop() = 0;
    void setViewDistance(int distance);
    void setSpeed(int speed);
    int distance = 0;
    int speed = 50;
};

#endif // AGENT_H
