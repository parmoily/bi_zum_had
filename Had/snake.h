#ifndef SNAKE_H
#define SNAKE_H

#include "field.h"

#include <QObject>

class Snake : public QObject
{
    Q_OBJECT
signals:
    void cellAdded(int h, int w);
    void cellDeleted(int h, int w);
    void newPos();
public:
    explicit Snake(QObject *parent = nullptr);
    explicit Snake(Field *in_field, QObject *parent = nullptr);

public slots:

    void create();
    bool hasPos(QPair<int, int> pos);
    bool exists();

    bool goUp();
    bool goRight();
    bool goLeft();
    bool goDown();
    int nextCellDirection(QPair<int, int> pos);

    void increaseSize();
    QPair<int, int> getHead();
    QList<QPair<int, int>> *getPositions();

    void makeRandomMove();
private:
    QList<QPair<int, int>> positions;
    Field *field = nullptr;
signals:

};

#endif // SNAKE_H
