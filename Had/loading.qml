import QtQuick 2.3
import QtQuick.Controls 1.2

ApplicationWindow {
    id: pageLoader
    width: 100; height: 100

    Image {
        id: pageLoaderimg
        z : 1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        source: "loading.png"
        visible : true
        anchors.fill: parent
    }
}
