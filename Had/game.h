#ifndef GAME_H
#define GAME_H

#include "agent.h"
#include "snake.h"

#include <QObject>

class Game : public QObject
{
    Q_OBJECT
public:
    explicit Game(QObject *parent = nullptr);
    explicit Game(Field* in_field, Snake* in_snake, QObject *parent = nullptr);
public slots:
    void initialize(int h, int w);
    void stop();
    void createFood(int count);

    void startRandomSnakeMovement();

    void snakeMoved();

    void setAgent(int id);
    void setDistance(int distance);
    void setSpeed(int speed);
    void setFoodsCount(int foodsCount);
private:
    Field *field = nullptr;
    Snake *snake = nullptr;
    Agent *agent = nullptr;
signals:
    void showError(QString errorString);
};

#endif // GAME_H
