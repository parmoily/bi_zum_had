import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.1

Window {
    id: window
    visible: true
    width: 700
    height: 480
    title: qsTr("Snake")
    property var bottomMarginDef: 100
    property var widthSq: 50
    property var heightSq: 50
    property var widthCount: 25
    property var heightCount: 25
    property var squereSideSize: Math.min(width / widthCount, (height - bottomMarginDef) / heightCount)

    MessageDialog {
        id: messageDialog
        title: "Warning"
        text: "It's so cool that you are using Qt Quick."
        onAccepted: {
        }
    }
    Item{
        id: fieldView

        Component.onCompleted: {
            var component;
            var sprite;
            component = Qt.createComponent("Field.qml");
            sprite = component.createObject(fieldView, {"rows": heightSq,
                                                "columns": widthSq});
            //children[0].rebuildField()
            console.log("squereSideSize :", squereSideSize)
            console.log("width :", width)
            console.log("height :", height)
        }

        function rebuildField()
        {
            for(var i = fieldView.children.length; i > 0 ; i--) {
                fieldView.children[i-1].destroy()
            }
            var component;
            var sprite;
            component = Qt.createComponent("Field.qml");
            sprite = component.createObject(fieldView, {"rows": heightSq,
                                                "columns": widthSq});

        }
    }

    RowLayout {
        id: settings
        height: 50
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15

        ColumnLayout {
            id: widthSliderLayout
            spacing: 2
            Layout.row: 1

            Text {
                id: element
                text: qsTr("Width") + "  " + Math.round(sliderW.value * widthSq)
                font.pixelSize: 12
            }

            Slider {
                id: sliderW
                width: 100
                height: 20
                Layout.preferredHeight: height
                Layout.preferredWidth: width
                value: 0.5
                stepSize : 0.1
                style: SliderStyle {
                    handle: Rectangle {
                        anchors.centerIn: parent
                        color: control.pressed ? "white" : "lightgray"
                        border.color: "gray"
                        border.width: 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 12
                    }
                }
            }
        }

        ColumnLayout {
            id: heightSliderLayout
            spacing: 2
            Layout.row: 2

            Text {
                id: element1
                text: qsTr("Height") + "  " + Math.round(sliderH.value * widthSq)
                font.pixelSize: 12
            }

            Slider {
                id: sliderH
                width: 100
                height: 20
                Layout.preferredHeight: height
                Layout.preferredWidth: width
                value: 0.5
                style: SliderStyle {
                    handle: Rectangle {
                        anchors.centerIn: parent
                        color: control.pressed ? "white" : "lightgray"
                        border.color: "gray"
                        border.width: 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 12
                    }
                }
            }
        }

        Button {
            id: recalculateFieldBtn
            text: qsTr("Recalculate")
            onClicked: {
                window.widthCount = Math.round(sliderW.value * widthSq)
                window.heightCount = Math.round(sliderH.value * widthSq)
                fieldView.rebuildField()
            }
        }

        Button {
            id: startBtn
            text: qsTr("Start")
            onClicked: {
                if(startBtn.text == "Start")
                {
                    game.initialize(heightCount, widthCount)
                    recalculateFieldBtn.enabled = false
                    sliderH.enabled = false
                    sliderW.enabled = false
                    buttonUp.enabled = false
                    buttonDown.enabled = false
                    buttonLeft.enabled = false
                    buttonRight.enabled = false
                    button.enabled = false
                    button1.enabled = false
                    button2.enabled = false

                    startBtn.text = "Stop"
                }
                else
                {
                    startBtn.text = "Start"
                    game.stop()
                }
            }
        }


        GridLayout {
            id: gridLayout
            width: 100
            height: 100
            rows: 2
            columns: 2

            Button {
                id: buttonUp
                text: qsTr("Up")

                onClicked: {
                    snake.goUp()
                }
            }

            Button {
                id: buttonDown
                text: qsTr("Down")

                onClicked: {
                    snake.goDown()
                }
            }

            Button {
                id: buttonLeft
                text: qsTr("Left")

                onClicked: {
                    snake.goLeft()
                }
            }

            Button {
                id: buttonRight
                text: qsTr("Right")

                onClicked: {
                    snake.goRight()
                }
            }
        }

        Connections {
            target: snake
            onCellAdded: {
                fieldView.children[0].snake(h, w)
            }

            onCellDeleted: {
                fieldView.children[0].field(h, w)
            }
        }

        Connections {
            target: field
            onFoodCreated: {
                fieldView.children[0].food(h, w)
            }
        }

        Connections {
            target: game
            onShowError: {
                messageDialog.text = errorString
                messageDialog.open()
            }
        }

    }

    ColumnLayout {
        id: columnLayout
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 10

        Label {
            id: label
            text: qsTr("Intelegent agent")
            Layout.fillHeight: false
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: false
        }

        RowLayout {
            id: rowLayout
            Layout.fillWidth: true

            ComboBox {
                id: comboBox
                model:["Reflex agent", "Goal Agent"]
                Layout.preferredWidth: 150
            }

            Button {
                id: button
                text: qsTr("Set")
                Layout.preferredWidth: 40
                onClicked: {
                    game.setAgent(comboBox.currentIndex)
                }
            }
        }

        Item{
            height: 35
        }

        Label {
            id: label1
            text: qsTr("Visible distance")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        RowLayout {
            id: rowLayout1
            Layout.fillWidth: true
            Layout.preferredWidth: parent.width

            SpinBox {
                id: spinBox
                width: 70
                maximumValue: 50
                minimumValue: 1
            }

            Button {
                id: button1
                Layout.preferredWidth: 40
                text: qsTr("Set")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                onClicked: {
                    game.setDistance(spinBox.value)
                }
            }
        }

        Item{
            height: 35
        }

        Label {
            id: label2
            text: qsTr("Speed")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        RowLayout {
            id: rowLayout2
            width: 100
            height: 100
            Layout.fillWidth: true
            Layout.preferredWidth: parent.width

            SpinBox {
                id: spinBox1
                width: 70
                maximumValue: 100
                minimumValue: 1
                value: 50
            }

            Button {
                id: button2
                Layout.preferredWidth: 40
                text: qsTr("Set")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                onClicked: {
                    game.setSpeed(spinBox1.value)
                }
            }
        }
        Item{
            height: 35
        }

        Label {
            id: label3
            text: qsTr("Foods count")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        RowLayout {
            id: rowLayout3
            width: 100
            height: 100
            Layout.fillWidth: true
            Layout.preferredWidth: parent.width

            SpinBox {
                id: spinBox2
                width: 70
                maximumValue: 3
                minimumValue: 1
                value: 1
            }

            Button {
                id: button3
                Layout.preferredWidth: 40
                text: qsTr("Set")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                onClicked: {
                    if(comboBox.currentIndex == 0)
                    {

                        messageDialog.text = "Multi-food field can have only Goal agent"
                        messageDialog.open()
                        return;
                    }

                    game.setFoodsCount(spinBox2.value)
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:2;anchors_x:6}
}
##^##*/
