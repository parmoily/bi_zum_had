#include "reflexagent.h"
#include <QtGlobal>
#include <QRandomGenerator>
#include <QDebug>

ReflexAgent::ReflexAgent(Field* in_field, Snake* in_snake, QObject *parent) : QObject(parent),
    field(in_field),
    snake(in_snake)
{
    this->distance = 0;
}

void ReflexAgent::makeDecision()
{
    calculateHeadDirection();


    /**
         * 0 - up
         * 1 - right
         * 2 - left
         * 3 - down
     */
    switch (headDirection){
        case 0  :
            snake->goUp();
            break;
        case 1  :
            snake->goRight();
            break;
        case 2  :
            snake->goLeft();
            break;
        default:
            snake->goDown();

    }

}

void ReflexAgent::start()
{
//    isRunning = true;
    timer = new QTimer(this);
        connect(timer, &QTimer::timeout, this, &ReflexAgent::makeDecision);
        timer->start(1500 / speed);
}

void ReflexAgent::stop()
{
//    isRunning = false;
    timer->stop();
    timer->deleteLater();
}

void ReflexAgent::updateKnownField()
{
    knownField.food = field->getFoodPos().at(0);
    knownField.head = snake->getPositions()->at(0);


    int headX = knownField.head.first;
    int headY = knownField.head.second;

    knownField.leftEdge = qMax(0, headY - distance);
    knownField.topEdge = qMax(0, headX - distance);

    knownField.rightEdge = qMin(field->getWidth() - 1, headY + distance);
    knownField.bottomEdge = qMin(field->getHeight() - 1, headX + distance);


}

void ReflexAgent::calculateHeadDirection()
{

    /**
         * 0 - up
         * 1 - right
         * 2 - left
         * 3 - down
     */
    if(snake->getPositions()->count(snake->getHead()) > 1)
    {
        stop();
        qDebug()<<"End ;";
    }

    headDirection = qrand() % 4;

    updateKnownField();

    if(foodInKnownField(knownField.food))
    {
        if(knownField.food.first < knownField.head.first)
        {
            headDirection = 0;
        }
        else if(knownField.food.first > knownField.head.first)
        {
            headDirection = 3;
        }
        else if(knownField.food.second < knownField.head.second)
        {
            headDirection = 2;
        }
        else if(knownField.food.second > knownField.head.second)
        {
            headDirection = 1;
        }

        if(!isValidDirection(headDirection))
        {
            headDirection = chooseRandomDirection();
        }
    }
    else
    {
        headDirection = chooseRandomDirection();
    }
}

bool ReflexAgent::isPosInKnownField(QPair<int, int> pos)
{
    if(pos.first <= knownField.topEdge)
    {
        return false;
    }
    else if (pos.first >= knownField.bottomEdge)
    {
        return false;
    }
    else if (pos.second <= knownField.leftEdge)
    {
        return false;
    }
    else if (pos.second >= knownField.rightEdge)
    {
        return false;
    }

    return true;
}

int ReflexAgent::chooseRandomDirection()
{
    int direction = headDirection;
    direction = qrand() % 4;
    int chooserCount = 0;
    while(!isValidDirection(direction))
    {
        direction = qrand() % 4;
        chooserCount++;

        if(chooserCount > 25)
        {
            stop();
            qDebug()<<"End";
            return -1;
        }
    }

    return direction;
}

bool ReflexAgent::isValidDirection(int direction)
{
    QPair<int, int> posByDirection = knownField.head;
    switch (direction){
        case 0  :
            posByDirection.first--;
            break;
        case 1  :
            posByDirection.second++;
            break;
        case 2  :
            posByDirection.second--;
            break;
        default:
            posByDirection.first++;

    }

    if(snake->getPositions()->indexOf(posByDirection) != -1)
    {
        return false;
    }

    switch (direction){
        case 0  :
            if(knownField.head.first == 0)
            {
                return false;
            }
            break;
        case 1  :
            if(knownField.head.second == field->getWidth() - 1)
            {
                return false;
            }
            break;
        case 2  :
            if(knownField.head.second == 0)
            {
                return false;
            }
            break;
        case 3  :
            if(knownField.head.first == field->getHeight() - 1)
            {
                return false;
            }
            break;
        default:
            return true;

    }


    return true;
}

int ReflexAgent::oppositeDirection(int direction)
{
    switch (direction){
        case 0  :
            return 3;
            break;
        case 1  :
            return 2;
            break;
        case 2  :
            return 1;
            break;
        default:
            return 0;

    }
}

bool ReflexAgent::foodInKnownField(QPair<int, int> pos)
{
    if(pos.first >= knownField.topEdge &&
            pos.first <= knownField.bottomEdge &&
            pos.second >= knownField.leftEdge &&
            pos.second <= knownField.rightEdge)
    {
        return true;
    }

    return false;
}
