import QtQuick 2.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14


Rectangle {
    property var cellSize: 30
    width: squereSideSize
    height: squereSideSize
    Layout.preferredWidth: cellSize
    Layout.preferredHeight: cellSize
    color: "lightgreen"
    border.color: "black"

    function snake(){
        color = "#ff3300"
    }
    function food(){
        color = "#ffff66"
    }
    function field(){
        color = "lightgreen"
    }
}
