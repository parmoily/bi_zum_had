#include "game.h"
#include "goalagent.h"
#include "reflexagent.h"
#include <QRandomGenerator>
#include <QDebug>
#include <QTimer>
#include <QtGlobal>

Game::Game(QObject *parent) : QObject(parent)
{

}

Game::Game(Field *in_field, Snake *in_snake, QObject *parent) :
    QObject(parent),
    field(in_field),
    snake(in_snake)

{
    connect(snake, SIGNAL(newPos()), this, SLOT(snakeMoved()));
}

void Game::initialize(int h, int w)
{
    if(snake->exists())
    {
        qDebug() << "Failed to initialize the game: Snake already exists";
        return;
    }

    if(agent == nullptr)
    {
        emit showError("Set Agent first");
        return;
    }

    if(agent->distance == 0)
    {
        emit showError("Set Agent view distance first");
        return;
    }

    field->setWidth(w);
    field->setHeight(h);
    snake->create();
    createFood(((GoalAgent *)agent)->getFoodsCount());
    agent->start();

//    snake->goUp();
//    snake->goUp();
//    snake->goUp();


    //    startRandomSnakeMovement();
}

void Game::stop()
{
    agent->stop();
}

void Game::createFood(int count)
{
    bool setted = false;
    int randX;
    int randY;

    for(int i = 0; i < count; i++)
    {
        while(!setted)
        {
            randX = qrand() % field->getHeight();
            randY = qrand() % field->getWidth();
            QPair<int, int> pos = QPair<int, int>(randX, randY);

            if(snake->hasPos(pos) || field->getFoodPos().indexOf(pos) != -1)
            {
                continue;
            }

            field->addFood(pos);
            setted = true;
        }
        setted = false;
    }
}

void Game::startRandomSnakeMovement()
{
    QTimer *timer = new QTimer(this);
        connect(timer, &QTimer::timeout, snake, &Snake::makeRandomMove);
        timer->start(3000);
}

void Game::snakeMoved()
{
    if(field->getFoodPos().indexOf(QPair<int, int>(snake->getHead().first,
                                   snake->getHead().second)) != -1)
    {
        snake->increaseSize();
        field->removeFood(snake->getHead());
        createFood(1);
    }
}

void Game::setAgent(int id)
{
    switch (id){
        case 0  :
            agent = new ReflexAgent(field, snake, this);
            break;
        case 1  :
            agent = new GoalAgent(field, snake, this);
            break;
        default:
            return;

    }

}

void Game::setDistance(int distance)
{
    if(agent == nullptr)
    {
        emit showError("Set Agent first");
        return;
    }

    agent->setViewDistance(distance);
    qDebug() << agent->distance;
}

void Game::setSpeed(int speed)
{
    if(agent == nullptr)
    {
        emit showError("Set Agent first");
        return;
    }

    agent->setSpeed(speed);
}

void Game::setFoodsCount(int foodsCount)
{
    ((GoalAgent *)agent)->setFoodsCount(foodsCount);
}
