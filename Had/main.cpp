#include "field.h"
#include "game.h"
#include "snake.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <qqmlcontext.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<Snake>("Snake", 1, 0, "Snake");
    qmlRegisterType<Field>("Field", 1, 0, "Field");
    qmlRegisterType<Game>("Game", 1, 0, "Game");


    QQmlApplicationEngine engine;

    Field *field = new Field();
    Snake *snake = new Snake(field);
    Game *game = new Game(field, snake);
    engine.rootContext()->setContextProperty("snake", snake);
    engine.rootContext()->setContextProperty("field", field);
    engine.rootContext()->setContextProperty("game", game);


    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
