import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.1

GridLayout {
    id: grid
    columnSpacing: 0
    rowSpacing: 0
    anchors.bottomMargin: 40
    rows: heightSq
    columns: widthSq

    function snake(x, y)
    {
        for(var i = grid.children.length; i > 0 ; i--) {
            var cell = grid.children[i-1]
            if(cell.Layout.row === x && cell.Layout.column === y)
            {
                cell.snake()
            }
        }
    }
    function field(x, y)
    {
        for(var i = grid.children.length; i > 0 ; i--) {
            var cell = grid.children[i-1]
            if(cell.Layout.row === x && cell.Layout.column === y)
            {
                cell.field()
            }
        }
    }
    function food(x, y)
    {
        for(var i = grid.children.length; i > 0 ; i--) {
            var cell = grid.children[i-1]
            if(cell.Layout.row === x && cell.Layout.column === y)
            {
                cell.food()
            }
        }
    }

    function rebuildField()
    {
        console.log("rebuildField :")
        for(var i = grid.children.length; i > 0 ; i--) {
            grid.children[i-1].destroy()
        }

        grid.update()
        //            grid.rebuildField()

        for(i = 0; i < heightCount; i++)
        {
            for(var j = 0; j < widthCount; j++)
            {
                var component;
                var sprite;
                component = Qt.createComponent("SquereCell.qml");
                sprite = component.createObject(grid, {"id": Number(i*10 + j),
                                                    "cellSize": squereSideSize,
                                                    "Layout.column" : j,
                                                    "Layout.row" : i});

            }
        }
    }

    Component.onCompleted: {
        rebuildField()
        console.log("squereSideSize :", squereSideSize)
        console.log("width :", width)
        console.log("height :", height)
    }


}

