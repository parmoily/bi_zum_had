#include "snake.h"
#include <QRandomGenerator>
#include <QDebug>

Snake::Snake(QObject *parent) :
    QObject(parent)
{

}

Snake::Snake(Field *in_field, QObject *parent) :
    QObject(parent)
{
    field = in_field;
}

void Snake::create()
{
    int randX = qrand() % field->getHeight();
    int randY = qrand() % field->getWidth();

    this->positions.append(QPair<int, int>(randX, randY));
    emit cellAdded(randX, randY);
}

bool Snake::hasPos(QPair<int, int> pos)
{
    return positions.indexOf(pos) > -1;
}

bool Snake::exists()
{
    return positions.size() > 0;
}

bool Snake::goUp()
{
    QPair<int, int> destination = QPair<int, int>(positions[0].first - 1, positions[0].second);

    if(destination.first < 0 ||
            destination.first >= field->getHeight() ||
            destination.second < 0 ||
            destination.second >= field->getWidth() ||
            hasPos(destination))
    {
        return false;
    }

    qDebug()<<"goUp()";

    positions.prepend(destination);

    QPair<int, int> last = positions.back();
    positions.pop_back();

    emit cellAdded(destination.first, destination.second);
    emit cellDeleted(last.first, last.second);

    emit newPos();
    return true;
}

bool Snake::goRight()
{
    QPair<int, int> destination = QPair<int, int>(positions[0].first, positions[0].second + 1);

    if(destination.first < 0 ||
            destination.first >= field->getHeight() ||
            destination.second < 0 ||
            destination.second >= field->getWidth() ||
            hasPos(destination))
    {
        return false;
    }
    qDebug()<<"goRight()";

    positions.prepend(destination);

    QPair<int, int> last = positions.back();
    positions.pop_back();

    emit cellAdded(destination.first, destination.second);
    emit cellDeleted(last.first, last.second);

    emit newPos();
    return true;
}

bool Snake::goLeft()
{
    QPair<int, int> destination = QPair<int, int>(positions[0].first, positions[0].second - 1);

    if(destination.first < 0 ||
            destination.first >= field->getHeight() ||
            destination.second < 0 ||
            destination.second >= field->getWidth() ||
            hasPos(destination))
    {
        return false;
    }
    qDebug()<<"goLeft()";

    positions.prepend(destination);

    QPair<int, int> last = positions.back();
    positions.pop_back();

    emit cellAdded(destination.first, destination.second);
    emit cellDeleted(last.first, last.second);

    emit newPos();
    return true;
}


bool Snake::goDown()
{
    QPair<int, int> destination = QPair<int, int>(positions[0].first + 1, positions[0].second);

    if(destination.first < 0 ||
            destination.first >= field->getHeight() ||
            destination.second < 0 ||
            destination.second >= field->getWidth() ||
            hasPos(destination))
    {
        return false;
    }

    qDebug()<<"goDown()";

    positions.prepend(destination);

    QPair<int, int> last = positions.back();
    positions.pop_back();

    emit cellAdded(destination.first, destination.second);
    emit cellDeleted(last.first, last.second);

    emit newPos();
    return true;
}

/**
 * @brief Snake::nextCellDirection
 * @return
     * 0 - up
     * 1 - right
     * 2 - left
     * 3 - down
 */
int Snake::nextCellDirection(QPair<int, int> pos)
{
    int nextIndex = positions.indexOf(pos) + 1;
    QPair<int, int> nextPos = positions.at(nextIndex);

    if(nextPos.first < pos.first)
    {
        return 0;
    }

    if(nextPos.first > pos.first)
    {
        return 3;
    }

    if(nextPos.second < pos.second)
    {
        return 2;
    }

    return 1;


}

void Snake::increaseSize()
{
    /**
         * 0 - up
         * 1 - right
         * 2 - left
         * 3 - down
     */
    int direction;
//    direction = QRandomGenerator::global()->bounded(4);
    if(positions.size() > 1)
    {
        direction = nextCellDirection(positions.at(positions.size() - 2));
    }
    else
    {
        direction = qrand() % 4;
    }

    QPair<int, int> lastPos = positions.back();
    bool setted = false;

    while (!setted)
    {
        QPair<int, int> newPos;


        switch (direction){
            case 0  :
                newPos = QPair<int, int>(lastPos.first - 1, lastPos.second);
                break;
            case 1  :
                newPos = QPair<int, int>(lastPos.first, lastPos.second + 1);
                break;
            case 2  :
                newPos = QPair<int, int>(lastPos.first, lastPos.second - 1);
                break;
            default:
                newPos = QPair<int, int>(lastPos.first + 1, lastPos.second);

        }

        if(newPos.first < 0 ||
                newPos.first >= field->getHeight() ||
                newPos.second < 0 ||
                newPos.second >= field->getWidth() ||
                hasPos(newPos))
        {
            direction = qrand() % 4;
            continue;
        }
        else
        {
            setted = true;
            positions.append(newPos);
            emit cellAdded(newPos.first, newPos.second);
        }
    }

}

QPair<int, int> Snake::getHead()
{
    return positions.front();
}

QList<QPair<int, int> > *Snake::getPositions()
{
    return &positions;
}

//void Snake::newPos()
//{
//    QPair<int, int> newPos = positions.at(0);

//    if(newPos == field->getFoodPos())
//    {

//    }
//}

void Snake::makeRandomMove()
{
    int rand = qrand() % 4;

    switch (rand){
        case 0  :
            goUp();
            break;
        case 1  :
            goRight();
            break;
        case 2  :
            goLeft();
            break;
        default:
            goDown();
            break;

    }
}
