#include "goalagent.h"
#include <QDebug>

GoalAgent::GoalAgent(Field* in_field, Snake* in_snake, QObject *parent) : QObject(parent),
    field(in_field),
    snake(in_snake)
{
    this->distance = 0;
}

void GoalAgent::setFoodsCount(int foodsCount)
{
    this->foodsCount = foodsCount;
}

void GoalAgent::makeDecision()
{

    calculateHeadDirection();

    /**
         * 0 - up
         * 1 - right
         * 2 - left
         * 3 - down
     */
    switch (headDirection){
        case 0  :
            snake->goUp();
            break;
        case 1  :
            snake->goRight();
            break;
        case 2  :
            snake->goLeft();
            break;
        default:
            snake->goDown();

    }

}

void GoalAgent::start()
{
//    isRunning = true;
    timer = new QTimer(this);
        connect(timer, &QTimer::timeout, this, &GoalAgent::makeDecision);
        timer->start(1500 / speed);

    for(int i = 0; i < field->getHeight(); i++)
    {
        QList<Point *> tmp;
        for(int j = 0; j < field->getWidth(); j++)
        {
            tmp.append(new Point());
        }
        m_field.append(tmp);
    }

}

void GoalAgent::stop()
{
//    isRunning = false;
    timer->stop();
    timer->deleteLater();
}

Point * startPoint;
Point* finishPoint;

void GoalAgent::calculateHeadDirection()
{

    /**
         * 0 - up
         * 1 - right
         * 2 - left
         * 3 - down
     */
    if(snake->getPositions()->count(snake->getHead()) > 1)
    {
        stop();
        qDebug()<<"End ;";
    }

    headDirection = qrand() % 4;

    updateKnownField();

    if(knownField.head.first == 3 && knownField.head.second == 6)
    {
        qDebug()<<" edfsdfds ";
    }

    if(isPosesInKnownField(knownField.foods)){
        int min = 1000;
        int minIdx = 0;
        for(int i = 0; i < knownField.foods.size(); i++)
        {
            if(isPosInKnownField(knownField.foods.at(i)))
            {
                finishPoint = m_field.at(knownField.foods.at(i).first)
                        .at(knownField.foods.at(i).second);
                int tmp = runSearch();
                if(min > runSearch())
                {
                    min = tmp;
                    minIdx = i;
                }
            }
        }
        finishPoint = m_field.at(knownField.foods.at(minIdx).first)
                .at(knownField.foods.at(minIdx).second);
        runSearch();
    }
    else
    {
        if(snake->getPositions()->size() == 1)
        {
            return;
        }

        int tries = 0;
        int hardFind = 0;
        do{
            tries++;

            if(tries > 20)
            {
                hardFind += 2;
                if(tries > 40)
                {
                    hardFind += 2;
                    if(tries > 60)
                    {
                        hardFind += 2;
                        if(tries > 80)
                            hardFind += 2;

                    }

                }
                if(tries > 100)
                {
                    qDebug()<<"tries > 100";
                    return;
                }
            }

            if(hardFind >= distance)
                break;

            int sideDirection = qrand() % 4;

            if(sideDirection == 0)
            {
                qDebug()<<"0";
                finishPoint = m_field.at(knownField.topEdge + hardFind)
                        .at(knownField.leftEdge + hardFind);
            }
            else if(sideDirection == 1)
            {
                qDebug()<<"1";
                finishPoint = m_field.at(knownField.topEdge + hardFind)
                        .at(knownField.rightEdge - hardFind);
            }
            else if(sideDirection == 2)
            {
                qDebug()<<"2";
                finishPoint = m_field.at(knownField.bottomEdge - hardFind)
                        .at(knownField.leftEdge + hardFind);
            }
            else if(sideDirection == 3)
            {
                qDebug()<<"3";
                finishPoint = m_field.at(knownField.bottomEdge - hardFind)
                        .at(knownField.rightEdge - hardFind);
            }
        } while(!surroundingsFree(QPair<int, int>(finishPoint->m_x, finishPoint->m_y)));

        runSearch();
    }
    qDebug()<<"found";

    QPair<int, int> nextCell = getNextCell();

    if(nextCell.first == -1 || nextCell.second == -1)
    {
        return;
    }
    else if(nextCell.first < knownField.head.first)
    {
        headDirection = 0;
    }
    else if(nextCell.first > knownField.head.first)
    {
        headDirection = 3;
    }
    else if(nextCell.second < knownField.head.second)
    {
        headDirection = 2;
    }
    else if(nextCell.second > knownField.head.second)
    {
        headDirection = 1;
    }

}

int GoalAgent::runSearch()
{
    startPoint = m_field.at(knownField.head.first)
            .at(knownField.head.second);

    int nodesExpanded = 0;
    int pathLen = 0;

    QList<Point *> buffer;
    QList<Point *> bufferTMP;
    QList<Point *> bufferTMP2;
    QList<Point *> bufferNeighbors;


    buffer.append(startPoint);
    startPoint->m_opened = true;
    startPoint->m_closed = false;
    nodesExpanded++;

    while(startPoint != finishPoint && buffer.size() < m_field.size() * m_field.at(0).size() &&  buffer.size() != 0)
    {
        pathLen++;

        for(auto it : buffer)
        {
            bufferNeighbors = getNeighbors(it);
            for(auto tmpIt : bufferNeighbors)
            {
                if( !tmpIt->m_opened && !tmpIt->m_closed)
                {
                    bufferTMP2.append(tmpIt);
                    tmpIt->parent = it;
                }
            }

            for(auto tmpIt : bufferTMP2)
            {
                tmpIt->m_opened = true;
                tmpIt->m_closed = false;
            }
            nodesExpanded+=bufferTMP2.size();

            bufferTMP.append(bufferTMP2);
            bufferNeighbors.clear();
            bufferTMP2.clear();
        }


        for(auto it : buffer){
            it->m_opened = false;
            it->m_closed = true;
        }
        buffer.clear();
        buffer.append(bufferTMP);
        for(auto it : buffer)
        {
            if(it == finishPoint){
                startPoint = it;
                return pathLen;
            }
        }

        bufferTMP.clear();
//        m_field->printField();
    }

    pathLen = 1000;
    return pathLen;
}

QPair<int, int> GoalAgent::getNextCell()
{
    int curX = finishPoint->m_x;
    int curY = finishPoint->m_y;
    startPoint = m_field.at(knownField.head.first)
            .at(knownField.head.second);

    while(m_field.at(curX).at(curY)->parent != startPoint)
    {
        if(m_field.at(curX).at(curY)->parent == nullptr)
        {
            return QPair<int, int>(-1, -1);
        }
        int tmpX = m_field.at(curX).at(curY)->parent->m_x;
        int tmpY = m_field.at(curX).at(curY)->parent->m_y;
        curX = tmpX;
        curY = tmpY;
    }

    return QPair<int, int>(curX, curY);
}

bool GoalAgent::isPosesInKnownField(QList<QPair<int, int>> poses)
{
    bool isIn = true;
    for(int i = 0; i < poses.size(); i++){
        isIn = true;
        if(poses.at(0).first < knownField.topEdge)
        {
            isIn = false;
        }
        else if (poses.at(0).first > knownField.bottomEdge)
        {
            isIn = false;
        }
        else if (poses.at(0).second < knownField.leftEdge)
        {
            isIn = false;
        }
        else if (poses.at(0).second > knownField.rightEdge)
        {
            isIn = false;
        }
        if(isIn == true)
            return true;
    }

    return false;
}

bool GoalAgent::isPosInKnownField(QPair<int, int> pos)
{
    if(pos.first <= knownField.topEdge)
    {
        return false;
    }
    else if (pos.first >= knownField.bottomEdge)
    {
        return false;
    }
    else if (pos.second <= knownField.leftEdge)
    {
        return false;
    }
    else if (pos.second >= knownField.rightEdge)
    {
        return false;
    }

    return true;
}

bool GoalAgent::surroundingsFree(QPair<int, int> pos)
{
    int sorroundingLen = 1;
    int l = pos.second - sorroundingLen;
    int r = pos.second + sorroundingLen;
    int t = pos.first - sorroundingLen;
    int b = pos.first + sorroundingLen;

    if(l < 0)
        return false;
    if(t < 0)
        return false;
    if(r >= field->getWidth())
        return false;
    if(b >= field->getHeight())
        return false;

    for(int i = t; i <= b; i++)
    {
        for(int j = l; j <= r; j++)
        {
            if(snake->hasPos(QPair<int, int>(i, j)))
                return false;
        }
    }

    return true;
}

QList<Point *> GoalAgent::getNeighbors(Point *point)
{
    QList<Point *> res;

    if(point->m_y + 1 <= knownField.rightEdge && !snake->hasPos(QPair<int, int>(point->m_x, point->m_y + 1)))
        res.append(m_field[point->m_x][point->m_y + 1]);
    if(point->m_x + 1 <= knownField.bottomEdge && !snake->hasPos(QPair<int, int>(point->m_x + 1, point->m_y)))
        res.append(m_field[point->m_x + 1][point->m_y]);
    if(point->m_y  > 0 && !snake->hasPos(QPair<int, int>(point->m_x, point->m_y - 1)))
        res.append(m_field[point->m_x][point->m_y - 1]);
    if(point->m_x > 0 && !snake->hasPos(QPair<int, int>(point->m_x - 1, point->m_y)))
        res.append(m_field[point->m_x - 1][point->m_y]);

    return res;
}

int GoalAgent::getFoodsCount() const
{
    return foodsCount;
}

void GoalAgent::updateKnownField()
{
    knownField.foods = field->getFoodPos();
    knownField.head = snake->getPositions()->at(0);


    int headX = knownField.head.first;
    int headY = knownField.head.second;

    knownField.leftEdge = qMax(0, headY - distance);
    knownField.topEdge = qMax(0, headX - distance);

    knownField.rightEdge = qMin(field->getWidth() - 1, headY + distance);
    knownField.bottomEdge = qMin(field->getHeight() - 1, headX + distance);

    for(int i = 0; i < field->getHeight(); i++)
    {
        for(int j = 0; j < field->getWidth(); j++)
        {
            m_field.at(i).at(j)->m_closed = false;
            m_field.at(i).at(j)->m_opened = false;
            m_field.at(i).at(j)->parent = nullptr;
            m_field.at(i).at(j)->m_x = i;
            m_field.at(i).at(j)->m_y = j;
        }
    }
}
