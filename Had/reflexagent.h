#ifndef REFLEXAGENT_H
#define REFLEXAGENT_H

#include "agent.h"
#include "snake.h"

#include <QObject>

#include <qtimer.h>


typedef struct
{
    int leftEdge;
    int rightEdge;
    int topEdge;
    int bottomEdge;
    QPair<int, int> food;
    QPair<int, int> head;

} HeadDirection;

class ReflexAgent : public QObject, public Agent
{
    Q_OBJECT
public :
    ReflexAgent(Field* in_field, Snake* in_snake, QObject *parent);
public slots:
    void makeDecision() override;
    void start() override;
    void stop() override;
    void updateKnownField();

    void calculateHeadDirection();
    bool isPosInKnownField(QPair<int, int> pos);
    int chooseRandomDirection();
    bool isValidDirection(int direction);
    int oppositeDirection(int direction);
    bool foodInKnownField(QPair<int, int> pos);
private:
    Field *field = nullptr;
    Snake *snake = nullptr;
//    bool isRunning = false;
    QTimer *timer = nullptr;
    HeadDirection knownField;

    int headDirection = -1;
};

#endif // REFLEXAGENT_H
