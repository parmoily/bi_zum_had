#ifndef GOALAGENT_H
#define GOALAGENT_H

#include "agent.h"
#include "snake.h"

#include <QObject>
#include <QList>

#include <qtimer.h>


#define MAX_INT 1000000

typedef struct
{
    int leftEdge;
    int rightEdge;
    int topEdge;
    int bottomEdge;
    QList<QPair<int, int>> foods;
    QPair<int, int> head;

} KnownField;

struct Point
{
    bool m_opened = false;
    bool m_closed = false;
    int m_x;
    int m_y;
    Point* parent = nullptr;
};

class GoalAgent : public QObject, public Agent
{
    Q_OBJECT
public:
    GoalAgent(Field* in_field, Snake* in_snake, QObject *parent);
    void setFoodsCount(int foodsCount);
    int getFoodsCount() const;

public slots:
    void makeDecision() override;
    void start() override;
    void stop() override;
    void updateKnownField();
    void calculateHeadDirection();
    int runSearch();
    QPair<int, int> getNextCell();
    bool isPosesInKnownField(QList<QPair<int, int>> poses);
    bool isPosInKnownField(QPair<int, int> pos);
    bool surroundingsFree(QPair<int, int> pos);
private:
    QList<Point *> getNeighbors(Point *point);

    Field *field = nullptr;
    Snake *snake = nullptr;
    QTimer *timer = nullptr;
    KnownField knownField;
    int headDirection = -1;
    int nodesExpanded = 0;
    int foodsCount = 1;
    QList<QList<Point *>> m_field;

};

#endif // GOALAGENT_H
